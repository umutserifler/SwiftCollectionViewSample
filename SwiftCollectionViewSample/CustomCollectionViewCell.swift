//
//  CustomCollectiovViewCellCollectionViewCell.swift
//  SwiftCollectionViewSample
//
//  Created by Umut Serifler on 12.02.2018.
//  Copyright © 2018 Umut Serifler. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var text: UILabel!
    
}
