//
//  ViewController.swift
//  SwiftCollectionViewSample
//
//  Created by Umut Serifler on 8.02.2018.
//  Copyright © 2018 Umut Serifler. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    
    @IBOutlet var collectionView: UICollectionView!
    
    let url = "https://restcountries.eu/rest/v1/all"
    
    var list:[MyStruct] = [MyStruct]()
    
    struct MyStruct {
        var countryName = ""
        var topLevelDomain = ""
        var alphaCode = ""
        
        init(_ data:NSDictionary) {
            
            if let add = data["name"] as? String{
                self.countryName = add
            }
            
            if let add = data["alpha3Code"] as? String{
                self.alphaCode = add
            }
            
            if let add = data["topLevelDomain"] as? NSArray
            {
                if let sub_item = add[0] as? String{
                    self.topLevelDomain = sub_item
                }
            }
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        
        get_data(url)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func get_data(_ link:String){
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data,response,error) in
            
            self.extract_data(data)
        })
        
        
        task.resume()
        
    }
    
    func extract_data(_ data:Data?){
        let json:Any?
        
        if(data == nil){
            return
        }
        
        do
        {
            json = try JSONSerialization.jsonObject(with: data!, options: [])
        }
        catch
        {
            return
        }
        
        
        guard let data_array = json as? NSArray else {
            return
        }
        for i in 0 ..< data_array.count
        {
            if let data_object = data_array[i] as? NSDictionary
            {
                list.append(MyStruct(data_object))
            }
        }
        
        refresh_now()
        
    }
    
    
    func refresh_now()
    {
        DispatchQueue.main.async(execute: {
            self.collectionView.reloadData()
        })
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
        //Do here
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
        
        let cellName = "customCVCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! CustomCollectionViewCell
        cell.text.text = list[indexPath.row].countryName
        cell.text.adjustsFontSizeToFitWidth = true
        
        //cell.backgroundColor = UIColor.blue
        
        return cell
        
        
        //Do here
    }
    
  //  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //Do here
  //  }
    


}

